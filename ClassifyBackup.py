import numpy as np
import pickle
from sklearn import neighbors, datasets


def reduceData(X):

    X = np.delete(X, 1, 1)
    X = np.delete(X, 1, 1)

    X = np.delete(X, 0, 2)
    X = np.delete(X, 0, 2)
    X = np.delete(X, 0, 2)

    return X

def reshapeData (set1, set2):

    X = np.zeros((2000, 5*2*3), dtype='f')

    y = np.zeros(shape=(2000), dtype='f')

    for i in range(0,1000):
        n = 0
        for j in range(0,5):
            for k in range (0,2):
                for m in range(0,3):
                    X[i, n] = set1[j, k, m, i]
                    X[i+1000, n] = set2[j, k, m ,i]

                    y[0:1000] = 3
                    y[1000:2000] = 4

                    n = n + 1
    return X, y


def centerData(X):

    allXCoordinates = X[:, :, 0, :]

    meanXValue = allXCoordinates.mean()

    X[:, :, 0, :] = allXCoordinates - meanXValue

    allYCoordinates = X[:, :, 1, :]

    meanYValue = allYCoordinates.mean()

    X[:, :, 1, :] = allYCoordinates - meanYValue

    allZCoordinates = X[:, :, 2, :]

    meanZValue = allZCoordinates.mean()

    X[:, :, 2, :] = allZCoordinates - meanZValue

    return X

################################################################################


np.set_printoptions(threshold=np.inf)

fileName = 'userData\\train3.dat'
f = open(fileName, 'rb')

train7 = pickle.load(f)
train7 = reduceData(train7)
train7 = centerData(train7)
f.close()

fileName = 'userData\\train4.dat'
f = open(fileName, 'rb')

train8 = pickle.load(f)
train8 = reduceData(train8)
train8 = centerData(train8)
f.close()

fileName = 'userData\\test3.dat'
f = open(fileName, 'rb')

test7 = pickle.load(f)
test7 = reduceData(test7)
test7 = centerData(test7)
f.close()


fileName = 'userData\\test4.dat'
f = open(fileName, 'rb')
test8 = pickle.load(f)
test8 = reduceData(test8)
test8 = centerData(test8)
f.close()

#########################################################

trainX, trainy = reshapeData(train7, train8)

testX, testy = reshapeData(test7, test8)

clf = neighbors.KNeighborsClassifier(15)
clf.fit(trainX,trainy)

correct = 0
for i in range(0, 2000):

    prediction = int(clf.predict([testX[i, :]]))

    print prediction, testy[i]

    if prediction == testy[i]:
        correct += 1

print (correct)
print (float(correct)/float(testy.size)*100)
