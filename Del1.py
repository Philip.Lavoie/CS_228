from pylab import *
from Leap import *

xMin = 1000.0
xMax = -1000.0
yMin = 1000.0
yMax = -1000.0



import random

controller = Controller()


ion()
show()

xPt = 0
yPt = 0

pt, = plot(xPt, yPt, 'ko', markersize=20)

while True:

    frame = controller.frame()

    if len(frame.hands) != 0:

        hand = frame.hands[0]
        fingers = hand.fingers
        index_finger_list = fingers.finger_type(Finger.TYPE_INDEX)
        indexFinger = index_finger_list[0]
        distalPhalanx = indexFinger.bone(Bone.TYPE_DISTAL)

        distalPhalanxPosition = distalPhalanx.next_joint

        x = distalPhalanxPosition[0]
        y = distalPhalanxPosition[1]

        if (x < xMin):
            xMin = x
        if (x > xMax):
            xMax = x

        if (y < yMin):
            yMin = y
        if y > yMax:
            yMax = y

        #print xMin, xMax, yMin, yMax

        xlim(xMin, xMax)
        ylim(yMin, yMax)

        pt.set_xdata(x)
        pt.set_ydata(y)

        pause(0.00001)
