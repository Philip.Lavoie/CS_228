from Leap import *
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import random
import sys
import pickle

xMin = 1000.0
xMax = -1000.0
yMin = 1000.0
yMax = -1000.0


class Deliverable:

    def __init__(self):

        self.numberOfGestures = 1000

        # 5 rows = fingers, 4 cols = bones, 6 stacks (xyz base, xyz tip)
        self.gestureData = np.zeros((5, 4, 6, self.numberOfGestures), dtype='f')

        self.gestureIndex = 0

        self.numberOfGesturesSaved = 0

        self.previousNumberOfHands = 0
        self.currentNumberOfHands = 0

        self.controller = Controller()
        self.lines = []

        matplotlib.interactive(True)

        self.fig = plt.figure(figsize=(8,6))
        self.ax = self.fig.add_subplot(111, projection='3d')

        self.ax.set_xlim(-250, 250)
        self.ax.set_ylim(-250, 250)
        self.ax.set_zlim(0, 250)

        self.ax.view_init(0, 90)

    def handleHands(self):

        self.previousNumberOfHands = self.currentNumberOfHands
        self.currentNumberOfHands = len(self.frame.hands)

        self.hand = self.frame.hands[0]

        for i in range (0, 5):

            self.handleFinger(i)

        plt.pause(0.00001)

        while (len(self.lines) > 0 ):
            ln = self.lines.pop()
            ln.pop(0).remove()
            del ln
            ln = []

        if (self.recordingIsEnding()):

            self.saveGesture()

            print('recording is ending')


        if (self.currentNumberOfHands == 2):

            print "gesture " + str(self.gestureIndex) + ' stored.'

            self.gestureIndex = self.gestureIndex + 1

            if (self.gestureIndex == self.numberOfGestures):

                print self.gestureData[:,:,:,0]
                print self.gestureData[:,:,:,99]

                self.saveGesture()

                sys.exit(0)


    def handleFinger(self, i):

        self.finger = self.hand.fingers[i]

        for j in range(0, 4):

            self.handleBone(i, j)

    def handleBone(self, i, j):

            if j == 0:
                bone = self.finger.bone(Bone.TYPE_METACARPAL)
            if j == 1:
                bone = self.finger.bone(Bone.TYPE_PROXIMAL)
            if j == 2:
                bone = self.finger.bone(Bone.TYPE_INTERMEDIATE)
            if j == 3:
                bone = self.finger.bone(Bone.TYPE_DISTAL)

            boneBase = bone.prev_joint
            boneTip = bone.next_joint

            xBase = boneBase[0]
            yBase = boneBase[1]
            zBase = boneBase[2]

            xTip = boneTip[0]
            yTip = boneTip[1]
            zTip = boneTip[2]

            if (self.currentNumberOfHands == 1):

                self.lines.append(self.ax.plot([-xBase,-xTip],[zBase,zTip],[yBase,yTip], "g"))
            else:
                self.lines.append(self.ax.plot([-xBase,-xTip],[zBase,zTip],[yBase,yTip], "r"))

            if (self.currentNumberOfHands == 2):

                self.gestureData[i, j, 0, self.gestureIndex] = xBase
                self.gestureData[i, j, 1, self.gestureIndex] = yBase
                self.gestureData[i, j, 2, self.gestureIndex] = zBase
                self.gestureData[i, j, 3, self.gestureIndex] = xTip
                self.gestureData[i, j, 4, self.gestureIndex] = yTip
                self.gestureData[i, j, 5, self.gestureIndex] = zTip


    def runOnce(self):

        self.frame = self.controller.frame()

        if len(self.frame.hands) != 0:

            self.handleHands()


    def runForever(self):

        while (True):

            self.runOnce()

    def recordingIsEnding(self):

        return (self.previousNumberOfHands == 2) & (self.currentNumberOfHands == 1)

    def saveGesture(self):

        fileName = 'userData\gesture.dat'
        f = open(fileName, 'wb')
        self.gestureData.dump(f)
        f.close()


deliverable = Deliverable()
deliverable.runForever()
