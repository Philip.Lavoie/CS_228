from Leap import *
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import random

xMin = 1000.0
xMax = -1000.0
yMin = 1000.0
yMax = -1000.0


class Deliverable:

    def __init__(self):

        # 5 rows = fingers, 4 cols = bones, 6 stacks (xyz base, xyz tip)
        self.gestureData = np.zeros((5, 4, 6), dtype='f')

        self.numberOfGesturesSaved = 0

        self.previousNumberOfHands = 0
        self.currentNumberOfHands = 0

        self.controller = Controller()
        self.lines = []

        matplotlib.interactive(True)

        self.fig = plt.figure(figsize=(8,6))
        self.ax = self.fig.add_subplot(111, projection='3d')

        self.ax.set_xlim(-250, 250)
        self.ax.set_ylim(-250, 250)
        self.ax.set_zlim(0, 250)

        self.ax.view_init(0, 90)

    def handleHands(self):

        self.previousNumberOfHands = self.currentNumberOfHands
        self.currentNumberOfHands = len(self.frame.hands)

        self.hand = self.frame.hands[0]

        for i in range (0, 5):

            self.handleFinger(i)

        plt.pause(0.00001)

        while (len(self.lines) > 0 ):
            ln = self.lines.pop()
            ln.pop(0).remove()
            del ln
            ln = []

        if (self.recordingIsEnding()):

            self.saveGesture()

            print('recording is ending')


    def handleFinger(self, i):

        self.finger = self.hand.fingers[i]

        for j in range(0, 4):

            self.handleBone(i, j)

    def handleBone(self, i, j):

            if j == 0:
                bone = self.finger.bone(Bone.TYPE_METACARPAL)
            if j == 1:
                bone = self.finger.bone(Bone.TYPE_PROXIMAL)
            if j == 2:
                bone = self.finger.bone(Bone.TYPE_INTERMEDIATE)
            if j == 3:
                bone = self.finger.bone(Bone.TYPE_DISTAL)

            boneBase = bone.prev_joint
            boneTip = bone.next_joint

            xBase = boneBase[0]
            yBase = boneBase[1]
            zBase = boneBase[2]

            xTip = boneTip[0]
            yTip = boneTip[1]
            zTip = boneTip[2]

            if (self.currentNumberOfHands == 1):

                self.lines.append(self.ax.plot([-xBase,-xTip],[zBase,zTip],[yBase,yTip], "g"))
            else:
                self.lines.append(self.ax.plot([-xBase,-xTip],[zBase,zTip],[yBase,yTip], "r"))

            if (self.recordingIsEnding()):

                self.gestureData[i, j, 0] = xBase
                self.gestureData[i, j, 1] = yBase
                self.gestureData[i, j, 2] = zBase
                self.gestureData[i, j, 3] = xTip
                self.gestureData[i, j, 4] = yTip
                self.gestureData[i, j, 5] = zTip

    def runOnce(self):

        self.frame = self.controller.frame()

        if len(self.frame.hands) != 0:

            self.handleHands()


    def runForever(self):

        while (True):

            self.runOnce()

    def recordingIsEnding(self):

        return (self.previousNumberOfHands == 2) & (self.currentNumberOfHands == 1)

    def saveGesture(self):

        self.numberOfGesturesSaved = self.numberOfGesturesSaved + 1

        fileName = 'userData\gesture' + str(self.numberOfGesturesSaved) + '.dat'
        f = open(fileName, 'wb')
        np.save(f, self.gestureData)
        f.close()

        fileName = 'userData\\numOfGesturesSaved.dat'
        f = open(fileName, 'wb')
        np.save(f, str(self.numberOfGesturesSaved))
        f.close()


deliverable = Deliverable()
deliverable.runForever()
