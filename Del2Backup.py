from Leap import *
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import random

xMin = 1000.0
xMax = -1000.0
yMin = 1000.0
yMax = -1000.0

controller = Controller()

lines = []

matplotlib.interactive(True)
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(111, projection='3d')

ax.set_xlim(-250, 250)
ax.set_ylim(-250, 250)
ax.set_zlim(0, 250)

ax.view_init(0, 90)


while True:

    frame = controller.frame()

    if len(frame.hands) != 0:

        hand = frame.hands[0]
        fingers = hand.fingers

        for finger in fingers:

            for j in range(0,4):

                if j == 0:
                    bone = finger.bone(Bone.TYPE_METACARPAL)
                if j == 1:
                    bone = finger.bone(Bone.TYPE_PROXIMAL)
                if j == 2:
                    bone = finger.bone(Bone.TYPE_INTERMEDIATE)
                if j == 3:
                    bone = finger.bone(Bone.TYPE_DISTAL)

                boneBase = bone.prev_joint
                boneTip = bone.next_joint

                xBase = boneBase[0]
                yBase = boneBase[1]
                zBase = boneBase[2]

                xTip = boneTip[0]
                yTip = boneTip[1]
                zTip = boneTip[2]

                lines.append(ax.plot([-xBase,-xTip],[zBase,zTip],[yBase,yTip],"r"))


        # fingers = hand.fingers
        # index_finger_list = fingers.finger_type(Finger.TYPE_INDEX)
        # indexFinger = index_finger_list[0]
        # distalPhalanx = indexFinger.bone(Bone.TYPE_DISTAL)
        #
        # distalPhalanxTipPosition = distalPhalanx.next_joint
        # distalPhalanxBasePosition = distalPhalanx.prev_joint
        #
        # xBase = distalPhalanxBasePosition[0]
        # yBase = distalPhalanxBasePosition[1]
        # zBase = distalPhalanxBasePosition[2]
        # xTip = distalPhalanxTipPosition[0]
        # yTip = distalPhalanxBasePosition[1]
        # zTip = distalPhalanxBasePosition[2]
        #
        # lines.append(ax.plot([xBase,xTip],[yBase,yTip],[zBase,zTip],"r"))

    plt.pause(0.00001)
    while ( len(lines) > 0 ):
        ln = lines.pop()
        ln.pop(0).remove()
        del ln
        ln = []
