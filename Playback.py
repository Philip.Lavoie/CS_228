import numpy as np
from Leap import *
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import random



class Reader:

    def __init__(self):

        self.lines = []

        matplotlib.interactive(True)

        self.fig = plt.figure(figsize=(8,6))
        self.ax = self.fig.add_subplot(111, projection='3d')

        self.ax.set_xlim(-250, 250)
        self.ax.set_ylim(-250, 250)
        self.ax.set_zlim(0, 250)

        self.ax.view_init(0, 90)


        self.numberOfGesturesSaved = 0

        fileName = 'userData/numOfGesturesSaved.dat'

        f = open(fileName, 'rb')

        self.numberOfGesturesSaved = np.load(f)

        print self.numberOfGesturesSaved

        f.close()


    def printGesture(self, i):

        fileName = 'userData/gesture' + str(i+1) + '.dat'

        print (fileName)

        f = open(fileName, 'rb')

        gestureData = np.load(f)

        print gestureData

        for i in range(0, 5):

            for j in range(0, 4):

                xBase = gestureData[i, j, 0]
                yBase = gestureData[i, j, 1]
                zBase = gestureData[i, j, 2]
                xTip = gestureData[i, j, 3]
                yTip = gestureData[i, j, 4]
                zTip = gestureData[i, j, 5]

                self.lines.append(self.ax.plot([-xBase,-xTip],[zBase,zTip],[yBase,yTip],"b"))

        plt.pause(0.5)

        while (len(self.lines) > 0 ):
            ln = self.lines.pop()
            ln.pop(0).remove()
            del ln
            ln = []

        f.close()

    def printData(self):

        for i in range(0, self.numberOfGesturesSaved):

            self.printGesture(i)

    def runForever(self):

        while (True):

            self.printData()



reader = Reader()
reader.runForever()
